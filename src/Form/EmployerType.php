<?php

namespace App\Form;

use App\Entity\Employer;
use App\Entity\Experience;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class,[
                'attr' => [
                    'placeholder' => 'Votre nom',
                    'class' => 'form-control'
                    ]
            ])
            ->add('prenom',TextType::class,[
                'attr' => [
                    'placeholder' => 'Votre prenon',
                    'class' => 'form-control'
                ]
            ])
            ->add('age',IntegerType::class,[
                'attr' => [
                    'placeholder' => 'Votre age',
                    'class' => 'form-control'
                ]
            ])
            ->add('poste',TextType::class,[
                'attr' => [
                    'placeholder' => 'Votre poste',
                    'class' => 'form-control'
                ]
            ])
            ->add('experience',EntityType::class,[
                'label' => 'choisir votre titre',
                'class' => Experience::class,
                'choice_label' => 'titre',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Employer::class,
        ]);
    }
}
